import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Navigation from './components/common/Navigation/Navigation';
import NotFoundComponent from './components/common/NotFound/NotFound';
import HomeComponent from './components/Home/Home';


const App = () => (
  <div>
    <Navigation />
    <Switch>
      <Route exact path="/" component={HomeComponent} />
      <Route path="/404" component={NotFoundComponent} />
      <Redirect to="/404" />
    </Switch>
  </div>
);

export default App;
