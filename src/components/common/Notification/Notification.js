import React from 'react';
import PropTypes from 'prop-types';

import './Notification.css';

class NotificationComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true
    };
  }
  componentDidMount() {
    if (!this.props.noClose) {
      setTimeout(() => {
        this.setState(() => ({
          isVisible: !this.state.isVisible
        }))
      }, 2000);
    }
  }
  render() {
    const { type, children } = this.props;
    const { isVisible } = this.state;
    return(
      <div 
        className={`
          alert 
          alert-${type} 
          notification
          ${!isVisible ? 'notification-hide' : ''}
        `} 
        role="alert"
      >
        {children}
      </div>
    );
  }
}

NotificationComponent.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  noClose: PropTypes.bool
};

export default NotificationComponent;