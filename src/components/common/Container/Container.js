import React from 'react';
import PropTypes from 'prop-types';

const ContainerComponent = ({ children }) => (
  <div className="container mt-2">
    <div className="row">
      {children}
    </div>
  </div>
);

ContainerComponent.propTypes = {
  children: PropTypes.node.isRequired
};

export default ContainerComponent;