import React from 'react';
import { Link } from 'react-router-dom';

const Navigation = () => (
  <nav className="navbar navbar-light bg-light border-bottom">
    <div className="container">
      <Link className="navbar-brand" to="/">Replaces</Link>
    </div>
  </nav>
);

export default Navigation;