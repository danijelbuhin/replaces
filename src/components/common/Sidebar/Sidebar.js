import React from 'react';
import PropTypes from 'prop-types';

import './Sidebar.css';

const SidebarComponent = ({ heading, children }) => (
  <div className="sidebar border rounded">
    <div className="sidebar-header">
      {heading}
    </div>
    <div className="sidebar-body">
      {children}
    </div>
  </div>
);

SidebarComponent.propTypes = {
  heading: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default SidebarComponent;