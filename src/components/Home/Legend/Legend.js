import React from 'react';
import SidebarComponent from '../../common/Sidebar/Sidebar';

import clusterIcon from '../../../assets/images/cluster.png';
import placeIcon from '../../../assets/images/place.png';

import './Legend.css';

const LegendComponent = () => (
  <SidebarComponent heading="Map legends">
    <span  className="legend">
      <img src={placeIcon} className="legend-icon" alt="Place icon" />
      <span className="legend-description">Place icon</span>
    </span>
    <span  className="legend">
      <img src={clusterIcon} className="legend-icon" alt="Multiple places icon" />
      <span className="legend-description">Multiple places (click to zoom)</span>
    </span>
  </SidebarComponent>
);

export default LegendComponent;