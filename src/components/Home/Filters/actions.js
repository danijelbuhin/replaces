import { 
  APPLY_FILTERS
} from "../../../actions";

export const applyFilters = (payload) => ({
  type: APPLY_FILTERS,
  payload
});