import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { applyFilters } from './actions';
import { isFetchingPlaces } from '../Map/actions';

import SidebarComponent from '../../common/Sidebar/Sidebar';

import './Filters.css';

class FiltersComponent extends React.Component {

  state = {
    radius: 2000,
    onlyOpen: false,
    rankBy: 'distance',
    showClusters: false,
  }

  handleOnSubmit = (e) => {
    e.preventDefault();
    const { radius, onlyOpen, rankBy, showClusters } = this.state;
    const data = {
      radius: parseFloat(radius),
      onlyOpen,
      rankBy,
      showClusters,
    }
    this.props.applyFilters(data);
    this.props.isFetchingPlaces(true);
  }

  render() {
    const { radius, rankBy, onlyOpen, showClusters } = this.state;
    const { isLocationProvided } = this.props;
    return (
      <SidebarComponent heading="Filters">
        <form onSubmit={(e) => this.handleOnSubmit(e)}>
          <div className="form-group">
            <label htmlFor="search-ranking">Rank by</label>
            <select 
              id="search-ranking" 
              className="form-control"
              value={rankBy}
              onChange={e => {
                e.persist();
                this.setState(() => ({
                  rankBy: e.target.value
                }));
              }}
            >
              <option value="distance">Distance (Closer to you)</option>
              <option value="popularity">Popularity (Inside the given radius)</option>
            </select>
          </div>
          {
            rankBy !== 'distance' &&
            <div className="form-group">
              <label htmlFor="search-radius">Radius (metres)</label>
              <input 
                type="number" 
                id="search-radius" 
                className="form-control" 
                placeholder={"Default is 2000"}
                value={radius}
                onChange={e => {
                  e.persist();
                  this.setState(() => ({
                    radius: e.target.value
                  }));
                }}
              />
            </div>
          }
          <div className="form-check mt-2 mb-2">
            <input 
              type="checkbox" 
              className="form-check-input" 
              id="filter-only-open-places"
              value={onlyOpen}
              onChange={e => {
                e.persist();
                this.setState(() => ({
                  onlyOpen: !onlyOpen
                }));
              }} 
            />
            <label 
              className="form-check-label" 
              htmlFor="filter-only-open-places"
            >
              Show only open places
            </label>
          </div>
          <div className="form-check mt-2 mb-2">
            <input
              type="checkbox"
              className="form-check-input"
              id="filter-show-clusters"
              value={showClusters}
              onChange={e => {
                e.persist();
                this.setState(() => ({
                  showClusters: !showClusters
                }));
              }}
            />
            <label
              className="form-check-label"
              htmlFor="filter-show-clusters"
            >
              Show Map Clusters
            </label>
          </div>
          <div className="form-group mt-1">
            <button 
              className="btn btn-primary btn-block"
              disabled={!isLocationProvided}
            >
              Apply
            </button>
          </div>
        </form>
      </SidebarComponent>
    );
  }
};

FiltersComponent.propTypes = {
  applyFilters: PropTypes.func.isRequired,
  isFetchingPlaces: PropTypes.func.isRequired,
  isLocationProvided: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ isLocationProvided }) => ({
  isLocationProvided
});

const mapDispatchToProps = dispatch => bindActionCreators({
  applyFilters,
  isFetchingPlaces
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FiltersComponent);