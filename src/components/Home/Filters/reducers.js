import { 
  APPLY_FILTERS
} from "../../../actions";

const filtersInitial = {
  radius: 2000,
  onlyOpen: false,
  rankBy: 'distance',
  showClusters: false,
};

const filters = (state = filtersInitial, { type, payload }) => {
  switch (type) {
    case APPLY_FILTERS:
      return payload;
    default: 
      return state;
  }
};

export default filters;