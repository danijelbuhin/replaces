import React from 'react';

import MapComponent from './Map/Map';
import ContainerComponent from '../common/Container/Container';
import FiltersComponent from './Filters/Filters';
import ResultsComponent from './Results/Results';
import LegendComponent from './Legend/Legend';

import './Home.css';

const HomeComponent = () => (
  <div>
    <MapComponent />
    <ContainerComponent>
      <div className="col-lg-9">
        <ResultsComponent />
      </div>
      <div className="col-lg-3">
        <FiltersComponent />
        <LegendComponent />
      </div>
    </ContainerComponent>
  </div>
);

export default HomeComponent;