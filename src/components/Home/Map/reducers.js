import { 
  RECEIVE_ALL_PLACES,
  LOADING_ALL_PLACES,
  IS_LOCATION_PROVIDED,
  GET_SELECTED_PLACE
} from '../../../actions';

const placesFetchedInitial = true;
export const placesFetched = (state = placesFetchedInitial, { type, payload }) => {
  switch (type) {
    case RECEIVE_ALL_PLACES:  
      return false;
    case LOADING_ALL_PLACES:
      return true;
    default:
      return state;
  }
};

const isLocationProvidedInitial = false;

export const isLocationProvided = (state = isLocationProvidedInitial, { type, payload }) => {
  switch (type) {
    case IS_LOCATION_PROVIDED:
      return payload;
    default: 
      return state;
  }
};

const selectedPlaceInitial = {};

export const selectedPlace = (state = selectedPlaceInitial, { type, payload }) => {
  switch (type) {
    case GET_SELECTED_PLACE:
      return payload;
    default:
      return state;
  }
};