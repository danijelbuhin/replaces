import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'; 
import OverlayLoader from 'react-overlay-loading/lib/OverlayLoader';

import { 
  isFetchingPlaces,
  pushPlaces,
  provideLocation,
  addSelectedPlace
 } from './actions';

import NotificationComponent from '../../common/Notification/Notification';
import InfoWindow from './InfoWindow/InfoWindow';

import * as mapStyles from './mapStyles.json';
import './Map.css';
import placeIcon from '../../../assets/images/place.png';

const google = window.google;
const MarkerClusterer = window.MarkerClusterer;

class MapComponent extends React.Component {

  state = {
    position: {
      lat: 0,
      lng: 0,
    },
    types: ['restaurant'],
    radius: 2000,
    filters: this.props.filters,
  };

  componentWillReceiveProps(nextProps) {
    this.setState(() => ({
      filters: nextProps.filters
    }));
  }

  componentDidUpdate() {
    this.initMap();
  }

  componentDidMount() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.setState(() => ({
          position: {
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
          }
        }), () => {
          this.initMap();
          this.props.provideLocation(true);
        });
      });
    }
  }
  
  initMap = () => {
    const { position } = this.state;
    this.map = new google.maps.Map(this.mapElement, {
      center: position,
      zoom: 15,
      styles: mapStyles,
      disableDefaultUI: true
    });
    this.initDefaultMarker();
    this.initPlaces();
  }

  initPlaces = () => {
    const { position, types, filters } = this.state;
    const { selectedPlace } = this.props;
    let searchArguments = {
      location: position,
      rankBy: google.maps.places.RankBy.DISTANCE,
      types
    };

    if (filters.rankBy === 'popularity') {
      searchArguments = {
        ...searchArguments,
        rankBy: google.maps.places.RankBy.PROMINENCE,
        radius: filters.radius,
      }
    }

    if (Object.keys(selectedPlace).length !== 0) {
      this.map.setCenter(selectedPlace.geometry.location);
      this.map.setZoom(18);
    }

    this.placesService = new google.maps.places.PlacesService(this.map);

    let places = [];
    let getMoreResults = null;

    if (this.moreResultsButton) {
      this.moreResultsButton.addEventListener('click', () => {
        this.moreResultsButton.disabled = true;
        this.moreResultsButton.textContent = 'Please wait...'
        if (getMoreResults) getMoreResults();
      });
    }

    this.placesService.nearbySearch(searchArguments, (results, status, pagination) => {

      if (status === 'ZERO_RESULTS') {
        this.props.pushPlaces([]);
      }

      places = [...places, ...results];

      if (this.state.filters.onlyOpen) {
        places = places.filter(place => {
          if (!place.opening_hours) return false;
          return place.opening_hours.open_now === true;
        });
      }

      if (!pagination.hasNextPage) {
        this.moreResultsButton.classList.add('hide');
      }
      this.moreResultsButton.disabled = !pagination.hasNextPage;
      this.moreResultsButton.textContent = 'Load more results';
      getMoreResults = pagination.hasNextPage && function () {
        pagination.nextPage();
      }

      let trimmedPlaces = places.filter((place, index, places) => {
        return places.findIndex(pl => pl.place_id === place.place_id && pl.name === place.name) === index;
      });

      this.props.pushPlaces(trimmedPlaces);

      let markers = trimmedPlaces.map(place => {

        let markerIcon = {
          url: placeIcon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        let marker = new google.maps.Marker({
          map: this.map,
          icon: markerIcon,
          title: place.name,
          position: {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
          }
        });

        marker.addListener('click', () => {
          this.props.addSelectedPlace(place);
          if (marker.getAnimation() != null) {
            marker.setAnimation(null);
          } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        });

        return marker;

      });

      if (this.props.filters.showClusters) {
        new MarkerClusterer(this.map, markers,
          { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' }
        );
      }

    });
  }

  initDefaultMarker = () => {
    let marker = new google.maps.Marker({
      map: this.map,
      position: this.state.position,
      title: 'Your current position',
      draggable: true
    });

    marker.addListener('dragend', (e) => {
      this.setState(() => ({
        position: {
          lat: e.latLng.lat(),
          lng: e.latLng.lng()
        }
      }), () => {
        this.initMap();
        this.props.provideLocation(true);
      });
    });

  }

  render() {
    const { isLocationProvided } = this.props;

    return (
      <div>
        <OverlayLoader
          loader="ClipLoader"
          text="Waiting for your location info"
          active={!isLocationProvided}
          backgroundColor="black"
          opacity=".9"
        >
          {
            isLocationProvided &&
            <button
              className="btn btn-primary load-more-button"
              ref={elem => this.moreResultsButton = elem}
            >
              Load more results
            </button>
          }
          <InfoWindow />
          <div id="map" ref={elem => this.mapElement = elem}></div>
        </OverlayLoader>
        {
          !isLocationProvided &&
          <NotificationComponent type="warning" noClose={true}>
            <strong>Warning!</strong> This app requires your location, please enable it in browser settings.
          </NotificationComponent>
        }
        {
          isLocationProvided &&
          <NotificationComponent type="primary">
            <strong>Great!</strong> Your location is enabled.
          </NotificationComponent>
        }
      </div>
    );
  }
}

const mapStateToProps = ({ selectedPlace, filters, isLocationProvided }) => ({
  filters,
  isLocationProvided,
  selectedPlace,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  isFetchingPlaces,
  pushPlaces,
  provideLocation,
  addSelectedPlace
}, dispatch);

MapComponent.propTypes = {
  pushPlaces: PropTypes.func,
  isFetchingPlaces: PropTypes.func,
  filters: PropTypes.object,
  provideLocation: PropTypes.func,
  addSelectedPlace: PropTypes.func,
  isLocationProvided: PropTypes.bool,
  selectedPlace: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(MapComponent);