import { 
  LOADING_ALL_PLACES, 
  RECEIVE_ALL_PLACES,
  GET_ALL_PLACES,
  IS_LOCATION_PROVIDED,
  GET_SELECTED_PLACE
} from "../../../actions";

export const isFetchingPlaces = (state) => {
  return state ? { type: LOADING_ALL_PLACES } : { type: RECEIVE_ALL_PLACES };
};

export const pushPlaces = (payload) => ({
  type: GET_ALL_PLACES,
  payload
});

export const provideLocation = (payload) => ({
  type: IS_LOCATION_PROVIDED,
  payload
});

export const addSelectedPlace = (payload) => ({
  type: GET_SELECTED_PLACE,
  payload
});