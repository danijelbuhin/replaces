import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './InfoWindow.css';
import placeIcon from '../../../../assets/images/place.png';

import { addSelectedPlace } from '../actions.js';

class InfoWindow extends React.Component {

  seatsStatus = () => {
    const availableSeats = Math.floor(Math.random() * (30 - 2 + 1)) + 2;
    return availableSeats;
  }

  render() {
    const { selectedPlace } = this.props;
    const isNotEmpty = Object.keys(selectedPlace).length !== 0 && selectedPlace.constructor === Object;
    let isVisible = isNotEmpty;
    return (
      <div className={`info-window${isVisible && isNotEmpty ? '' : ' hide'}`}>
        { 
          isNotEmpty &&
          <React.Fragment>
            <div className="info-window-details">
              <p className="info-window-name">
                <img src={placeIcon} alt="Place" className="info-window-name-icon" />
                {selectedPlace.name}
              </p>
              <p className="info-window-detail">
                <i className="ti-location-pin info-window-icon" />
                {selectedPlace.vicinity}
              </p>
              <span className="info-window-detail">
                <i className="ti-agenda info-window-icon" /> 
                {this.seatsStatus() + ' / 30'} seats reserved
              </span>
            </div>
            <div className="info-window-buttons">
              <button 
                className="btn btn-block"
                onClick={() => {
                  isVisible = false;
                  this.props.addSelectedPlace({})
                }}
              >
                Close
              </button>
            </div>
          </React.Fragment>
        }
      </div>
    );
  }

};

const mapStateToProps = ({ selectedPlace }) => ({
  selectedPlace
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addSelectedPlace
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(InfoWindow);