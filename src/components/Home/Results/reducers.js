import { 
  GET_ALL_PLACES,
} from '../../../actions';

const allPlacesInitial = [];
const allPlaces = (state = allPlacesInitial, { type, payload }) => {
  switch (type) {
    case GET_ALL_PLACES:
      return payload.sort((a, b) => {
        if (!a.rating) return 1;
        if (!b.rating) return -1;
        return b.rating - a.rating;
      });
    default:
      return state;
  }
};

export default allPlaces;