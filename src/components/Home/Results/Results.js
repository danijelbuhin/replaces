import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { 
  addSelectedPlace
 } from '../Map/actions';

import './Results.css';

const ResultsComponent = ({ places, addSelectedPlace }) => {
  return (
    <table className="table table-hover table-bordered">
      <thead className="thead-light">
        <tr>
          <th scope="col"></th>
          <th scope="col">Name</th>
          <th scope="col">Address</th>
          <th scope="col">Rate</th>
        </tr>
      </thead>
      <tbody>
        {places.length !== 0 && places.map((place, index) => (
          <tr key={index}>
            <td>{index + 1}</td>
            <td>
              <p
                className="place-name"
                onClick={() => {
                  addSelectedPlace(place);
                }}
              >
                {place.name}
              </p>
              {
                !place.opening_hours &&
                <span className="place-status">
                  <i 
                  className="ti-help-alt"
                  title="Opening hours are unknown" />
                  Unknown opening hours
                </span>
              }
              {
                place.opening_hours && !place.opening_hours.open_now &&
                <span className="place-status">
                  <i 
                    className="ti-face-sad" 
                    title="Place is closed at this time" />
                  Currently closed
                </span>
              }
              {
                place.opening_hours && place.opening_hours.open_now &&
                <span className="place-status">
                  <i 
                    className="ti-time" 
                    title="Place is opened at this time" />
                  Open
                </span>
              }
              {
                place.photos &&
                <span className="place-status">
                  <i 
                    className="ti-camera"
                    title="Photos are presented" />
                  Photos presented
                </span>
              }
              {
                !place.photos &&
                <span className="place-status">
                  <i
                    className="ti-image"
                    title="Photos are not presented" />
                  No photos
                </span>
              }
              
            </td>
            <td>{place.vicinity}</td>
            <td>{place.rating || '/'}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

const mapDispatchToProps = dispatch => bindActionCreators({
  addSelectedPlace,
}, dispatch);

const mapStateToProps = ({ places }) => ({
  places
});

ResultsComponent.propTypes = {
  places: PropTypes.arrayOf(PropTypes.object),
  addSelectedPlace: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultsComponent);