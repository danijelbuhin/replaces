import { combineReducers } from 'redux';

import places from './components/Home/Results/reducers';
import filters from './components/Home/Filters/reducers';
import { placesFetched, isLocationProvided, selectedPlace } from './components/Home/Map/reducers';

export default combineReducers({
  places,
  filters,
  placesFetched,
  isLocationProvided,
  selectedPlace
});